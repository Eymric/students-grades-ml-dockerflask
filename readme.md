## API FLASK : EYMRIC GATIBELZA 

# Pour l'upload csv, possibilité d'upload 'student-mat.csv' et 'student-por.csv

* @app.route("/")
Renvoies à la page d'accueil

* @app.route("/pred")
Renvoies à la prediction d'un étudiant

* @app.route("/prediction", methods=['POST'])
def prediction():
Envoi de la pred 

* @app.route('/train')
Renvoies à la page pour entrainer le modele

* @app.route("/entrainement", methods=['POST'])
Renvoies au resultat de l'entrainement

* @app.route('/upload')
Renvoies à la page d'upload de fichier (student-mat.csv ou student-por.csv)

* @app.route("/uptrain", methods=['POST'])
Renvoies la réponse du fichier telechargé