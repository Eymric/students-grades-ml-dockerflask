import pandas as pd
import seaborn as sns
import sys
from pandas import read_csv
from pandas import DataFrame
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
import pickle
from flask_bootstrap import Bootstrap
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import os



def binaryFields(dataset):
    school = {'GP': 0, 'MS': 1}
    sex = {'F': 0, 'M': 1}
    address = {'R': 0, 'U': 1}
    famsize = {'LE3': 0, 'GT3': 1}

    dataset['school'] = [school[i] for i in dataset['school']]
    dataset['sex'] = [sex[i] for i in dataset['sex']]
    dataset['address'] = [address[i] for i in dataset['address']]
    dataset['famsize'] = [famsize[i] for i in dataset['famsize']]
    dataset['romantic'] = [1 if i == "yes" else 0 for i in dataset['romantic']]
    dataset['internet'] = [1 if i == "yes" else 0 for i in dataset['internet']]
    dataset['Pstatus'] = [1 if i == "A" else 0 for i in dataset['Pstatus']]
    dataset['schoolsup'] = [1 if i == "yes" else 0 for i in dataset['schoolsup']]
    dataset['famsup'] = [1 if i == "yes" else 0 for i in dataset['famsup']]
    dataset['paid'] = [1 if i == "yes" else 0 for i in dataset['paid']]
    dataset['activities'] = [1 if i == "yes" else 0 for i in dataset['activities']]
    dataset['nursery'] = [1 if i == "yes" else 0 for i in dataset['nursery']]
    dataset['higher'] = [1 if i == "yes" else 0 for i in dataset['higher']]
    dataset.drop(labels="G1", axis=1, inplace=True)
    dataset.drop(labels="G2", axis=1, inplace=True)
    dataset['G3'] = [1 if i > 9 else 0 for i in dataset['G3']]
    return dataset


def trainModel(filename=None):
    if filename is None:
        dataset = pd.read_csv('student-mat.csv')
    else :
        dataset = pd.read_csv('data/'+filename)

    school = {'GP': 0, 'MS': 1}
    sex = {'F': 0, 'M': 1}
    address = {'R': 0, 'U': 1}
    famsize = {'LE3': 0, 'GT3': 1}

    dataset = binaryFields(dataset)

    X = dataset[
        ['paid', 'internet', 'higher', 'famsup', 'famsize', 'goout', 'G3', 'activities', 'schoolsup', 'romantic',
         'Pstatus', 'address']].dropna(subset=['G3'])
    y = X['G3'].copy()
    del X['G3']
    x_train, x_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=0)
    reg = RandomForestRegressor(max_depth=14, n_estimators=50, random_state=0).fit(X, y)
    accuracy = reg.score(x_val, y_val)
    # save the model to disk
    filename = 'cls.pkl'
    pickle.dump(reg, open(filename, 'wb'))

    return 'La precision est de : {0}% pour {1} features'.format(int(accuracy * 100), len(X))



def predict(fields):
    # load the model from disk
    fields = pd.DataFrame(data=fields, index=[0])
    filename = 'cls.pkl'
    cls = pickle.load(open(filename, 'rb'))
    predict = cls.predict(fields)
    if predict * 100 >= 50:
        return 'Il aura son baccalauréat'
    else:
        return "Il n'aura pas son baccalauréat"

app = Flask(__name__)


@app.route("/")
def home():
    return render_template("index.html", title='Home')

@app.route("/pred")
def predicition():
    return render_template("prediction.html", title='prediction')

@app.route("/prediction", methods=['POST'])
def prediction():
    fields = request.form.to_dict()
    print(fields, file=sys.stderr)
    return predict(fields)

@app.route('/train')
def train():
    return render_template("entrainement.html")

@app.route("/entrainement", methods=['POST'])
def entrainement():
    return render_template("result.html", message=trainModel())

@app.route('/upload')
def upload():
    return render_template("upload.html")

@app.route("/uptrain", methods=['POST'])
def upload_train():
    if request.method =='POST':
        file = request.files['file[]']
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join("data", filename))
    return render_template("result.html", message=trainModel(filename))

if __name__ == "__main__":
    Bootstrap(app)
    app.run(debug=True, host='0.0.0.0')
